# Usage Terms

The data provided on the website [agrineer.org](https://agrineer.org) is freely available to the public given the restrictions below. Re-distribution is permitted as long as attribution is given to agrineer.org and that the restrictions below are explicitely reiterated and enforced.
  
(Our preference, though, is to refer the user to our sector download page for analytics purposes.) 

Please read the disclaimer and privacy policies in the website footer.

Downloading agrineer.org's data files means you agree NOT to:

   - make the data available for a charge.
   - compile into databases or websites to which only registered members or customers can have access.
   - use a proprietary or closed technology or encryption which creates a barrier for open access to the data and to the application software source code.
   - copyright forbidding (or obfuscating) re-use of the data, including the use of "no derivatives" requirements.
   - patent forbidding re-use of the data (for example the 3-dimensional coordinates of some experimental protein structures have been patented).
   - restrict the use of robots to websites, with preference to certain search engines.
   - aggregate data into "databases" which may be covered by "database rights" or "database directives" (e.g. Directive on the legal protection of databases).
   - time-limit access to resources such as e-journals (which on traditional print were available to the purchaser indefinitely).

