#! /usr/bin/env /usr/bin/python3

#  get_input_files.py
# 
#  Copyright (c) 2018 IndieCompLabs, LLC.
# 
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  (at your option) any later version.
# 
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
# 
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#  or visit https://www.gnu.org/licenses/gpl-3.0-standalone.html

get_input_files_copyright = 'get_input_files.py Copyright (c) 2018 IndieCompLabs, LLC., released under GNU GPL V3.0'

import os
import sys
import getopt
#import datetime

from agrineer import wrfbase
## @file    get_input_files.py
## @brief   Program to download WRF/ETo input data for GDC and SME applications.
## @detail  This program creates and populates the WRF input directory
##          for the GDC and SME applications, sourced from the Agrineer.org 
##          server. Run this program once then use "update_input_files.py" in 
##          a cron job to append new data on a daily basis.

## @author    Scott L. Williams
## @copyright Copyright (c) 2018 IndieCompLabs, LLC. All Rights Reserved.
## @license   Released under GNU General Public License V3.0.

## Usage report
def usage():
    print('usage: get_input_files.py', file=sys.stderr,flush=True)
    print('       -h, --help', file=sys.stderr,flush=True)
    print('       -d dir, --dir=dir', file=sys.stderr,flush=True)
    print('       -y yyyy, --year=yyyy', file=sys.stderr,flush=True)

## Parse the command line for arguments.
## @return year and data directory
def set_params( argv ):
    year = None
    datadir = None
    
    try:                                
        opts, args = getopt.getopt( argv, 'hy:d:', ['help', 'year=', 'dir='] )
    except:
        usage()                          
        sys.exit(2)
                   
    for opt, arg in opts:
        print( opt, arg )
        if opt in ( '-h', '--help' ):      
            usage()                     
            sys.exit(2)
            
        if opt in ( '-y', '--year' ):
            year = arg

        if opt in ( '-d', '--dir' ):
            datadir = arg

    if year == None:
        print( 'Must specify year', file=sys.stderr, flush=True )
        usage()
        sys.exit(1)

    if dir == None:
        print( 'Must specify data directory', file=sys.stderr, flush=True )
        usage()
        sys.exit(1)

    return year, datadir

## grab static geo files
def get_static( directory ):

    # check if directory is there
    if os.path.isdir( directory ):
        print( 'data directory ' + directory + ' already exists, exiting' )
        sys.exit(1)
    
    # else create
    os.mkdir( directory )
    if not os.path.isdir( directory ):
        print( 'data directory ' + directory + ' cannot be made, exiting' )
        sys.exit(1)

    os.chdir( directory )
    
    # snag static zip file from server
    os.system( 'wget https://agrineer.org/SMVstatic.zip' )
    os.system( 'unzip SMVstatic.zip' )
    os.system( 'rm SMVstatic.zip' )

def get_sector( directory, sector, year ):
    
    os.mkdir( directory + '/archive/' + sector )
    os.chdir( directory + '/archive/' + sector )
    
    # snag the sector yearly zip
    os.system( 'wget https://agrineer.org/SMVdataZyearly/' + sector + '/' + year + '.zip' )
    os.system( 'unzip ' + year + '.zip' )
    os.system( 'rm ' + year + '.zip' )

# main routine    
# sectors to download can be selected by editing the "sectors" array below.
if __name__ == '__main__':  

    year,datadir = set_params( sys.argv[1:] )  
    print('Data directory: ', datadir,
          file=sys.stderr,flush=True)
    print('Year: ', year,
          file=sys.stderr,flush=True)
   
    get_static( datadir )              # grab the geo files

    os.mkdir( datadir + '/archive' )   # setup the archive dir

    # array of sectors
    # TODO: have server report active sectors
    # TODO: address multiple years 
    
    sectors = [ 'WCONUS_B1',
                'WCONUS_B2',
                'WCONUS_B3',
                'WCONUS_B4',
                'WCONUS_C1',
                'WCONUS_C2',                  
                'WCONUS_D1',
                'WCONUS_D2',
                'WCONUS_D3',
                'WCONUS_E2' ]

    # populate sectors
    for sector in sectors:
        get_sector( datadir, sector, year )

# end __main__

# end get_input_files.py
