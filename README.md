## DESCRIPTION
This is a catch-all package providing utility programs and classes used in some of Agrineer's applications.

This release provides Python 3 modules used in the standalone [Grow Degree Day (GDC)](https://gitlab.com/agrineer/gdc) and [Soil Moisture Estimator (SME)](https://gitlab.com/agrineer/sme) applications. This package must be installed and data downloaded prior to using these applications.

The focus of this package is the *wrfbase* class which reads and delivers NetCDF4 climate data, generated from the Weather, Research, and Forecast model (WRF) and from our evaporation calculations using the UN's Food and Agriculture Organization's (FAO) guidelines. See Agrineer.org's [ETo](https:gitlab.com/agrineer/ETo) package for implementation details on this input data.

Installation of this package and downloading of the data is prerequisite for the stand alone GDC and SME applications. Please review terms regarding usage of data [here](https://www.agrineer.org/downloads/dataterms.php) or in the DATA_TERMS_OF_USAGE.md file included in this package. Once this package and data are installed you can use the applications GDC and SME as stand alone programs.

## DEPENDENCIES

This package is known to work with Linux Mint 17,18,19 and Ubuntu 14 operating systems.

Install the prerequisites packages (use apt-get or Synaptic):
eg. > sudo apt-get install package-name

 - build-essential
 - python3-dev
 - python3-numpy
 - python3-pil
 - python3-pip
 - cython3
 - libhdf5-dev
 - libnetcdf-dev 

Install the netCDF4 python package by the pip3 command:
  
 - pip3 install wheel
 - pip3 install setuptools
 - pip3 install netCDF4
    

## INSTALL


  - Select or make a target directory in your home space and 'cd' to it, eg.
      
          > cd /home/user/MyPackages  
  then clone the package from GitLab,
     
          > git clone https://gitlab.com/agrineer/agrineer  
  Or go to [https://gitlab.com/agrineer/agrineer](https://gitlab.com/agrineer/agrineer) and download the zip or tar version and unpack in the target directory.   
  
  - Point to target directory in your PYTHONPATH environment variable eg.  
  
          > export PYTHONPATH=/home/you:$PYTHONPATH

  - To run programs from the package, include the directory "agrineer" in your PATH environment variable eg. 
  
          > export PATH=/home/user/MyPackages/agrineer:$PATH

      or address them directly.
 

NOTE:   
Data files are necessary to run the SME and GDC applications and this package provides programs to easily download the archived data and also to increment the data on a daily basis.

### How to get data files  

Agrineer.org generates daily WRF/ETo files for active sectors and are freely available as zip files on a daily, monthly, or yearly basis [here](https://www.agrineer.org/downloads/sectors.php). 
    
    
You can download these files manually through our server or by programs supplied in this package (recommended).

To use the provided programs:

Determine a directory path to use for input data to GDC or SME applications and use the program 'get_input_files.py'. For example,


- go to 'agrineer' package directory eg.   
      > cd /home/user/MyPackages/agrineer  
  
- run the download program, eg.  
      > ./get_input_files.py -d /home/user/SMVdata -y 2018

This will create the directory /home/user/SMVdata containing static geo data and up-to-date sector data for the year 2018.   

Four sub-directories will be created:
	archive, static_30km, static_10km, and static_3km.

Where:

- the archive directory holds the active sector directories,   
eg. WCONUS_B1, etc.    
- the static_* dirs hold domain geo information for each sector.  
 eg. static_3km: WCONUS_B1_geo_d02.nc, ..., WCONUS_E2_geo_d02.nc.

After running 'get_input_files.py' you can add data files on a daily basis by running the program 'update_input_files.py', eg.

      > ./update_input_files.py -d /home/user/SMVdata

or you can include the command in a cron job. Please refer to the program's documentation for command line arguments, ie. for a particular date, use -r YYYYMMDD

To check if necessary files are available locally, run 'check_files.py'. This program exercises the package classes used in GDC and SME.

For example, to check for files from 01/01/2018 to latest date, use:

       > ./check_files.py -d /home/user/SMVdata -s 20180101 

Output should look like:

Running from  20180101  to  20180614  
sector: WCONUS_B1 ok.  
sector: WCONUS_B2 ok.  
sector: WCONUS_B3 ok.  
sector: WCONUS_B4 ok.  
sector: WCONUS_C1 ok.  
sector: WCONUS_C2 ok.  
sector: WCONUS_D1 ok.  
sector: WCONUS_D2 ok.    
sector: WCONUS_D3 ok.  
sector: WCONUS_E2 ok.  

Once you get check_files.py to run as above, you can install the [Grow Degree Day (GDC)](https://gitlab.com/agrineer/gdc) and [Soil Moisture Estimator (SME)](https://gitlab.com/agrineer/sme) applications.

## Documentation

	You can find class documentation by pointing your browser to agrineer/doc/index.html.
	A Doxygen file is provided for updating the documentation. To update:
	- Go to installed "agrineer" directory
	- Run doxygen, eg.
	  > doxygen
	    This will produce/update the directory "doc". 
 	    Browse doc/index.html for class and program information.



