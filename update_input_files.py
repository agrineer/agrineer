#! /usr/bin/env /usr/bin/python3

#  update_input_files.py
# 
#  Copyright (c) 2018 IndieCompLabs, LLC.
# 
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  (at your option) any later version.
# 
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
# 
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#  or visit https://www.gnu.org/licenses/gpl-3.0-standalone.html

update_input_files_copyright = 'update_input_files.py Copyright (c) 2018 IndieCompLabs, LLC., released under GNU GPL V3.0'

import os
import sys
import getopt
import datetime

from agrineer import wrfbase

## @file    update_input_files.py
## @brief   Program to update WRF/ETo input data on a daily basis.
## @detail  This program updates the WRF input directory
##          for the GDC and SME applications, sourced from the Agrineer.org 
##          server. You must first run 'get_input_file.py' once to establish 
##          past data files, then run this program daily (eg. in a cron job) to 
##          append new data on a each sector.\n
##          For daily implementation use:\n
##          'update_input_files.py -d /home/agrineer/SMVdata',\n using your own
##          data directory path. \n
##          For filling in dates use:\n 
##          'update_input_files.py -d /home/agrineer/SMVdata -a 20180429'.
##
## @author    Scott L. Williams
## @copyright Copyright (c) 2018 IndieCompLabs, LLC. All Rights Reserved.
## @license   Released under GNU General Public License V3.0.

## Usage report.
def usage():
    print('usage: update_input_files.py', file=sys.stderr,flush=True)
    print('       -h, --help', file=sys.stderr,flush=True)
    print('       -a yyyymmdd, --date=yyyymmdd', file=sys.stderr,flush=True)
    print('       -d dir, --dir=dir', file=sys.stderr,flush=True)

## Parse the command line for arguments.
## @return date and data directory given in command line, if no date is given then the  day before yesterday's date is calculated.
def set_params( argv ):
    date = None
    datadir = None
    
    try:                                
        opts, args = getopt.getopt( argv, 'ha:d:', ['help', 'date=', 'dir='] )
    except:
        usage()                          
        sys.exit(1)
                   
    for opt, arg in opts:                
        if opt in ( '-h', '--help' ):      
            usage()                     
            sys.exit(1)
            
        elif opt in ( '-a', '--date' ):
            date = arg

        elif opt in ( '-d', '--dir' ):
            datadir = arg

    if date == None:
        # get day before yesterday's date
        today = datetime.datetime.now()
        day_before = today - datetime.timedelta(days = 2)
        date = day_before.strftime( "%Y%m%d" )

    if datadir == None:
        print( 'Must specify data directory', file=sys.stderr, flush=True )
        sys.exit(1)

    return date, datadir

## Snag the sector daily zip from agrineer.org.
def get_update( directory, sector, date ):
    
    os.chdir( directory + '/' + sector )

    zipname =  sector + '_' + date + '.zip'
    os.system( 'wget https://agrineer.org/SMVdataZdaily/' + sector + '/'
               + zipname )
    os.system( 'unzip ' + zipname )
    os.system( 'rm ' + zipname )

    print( sector + ' updated.', file=sys.stderr, flush=True )

# main routine    
# sectors to download can be selected by editing the "sectors" array below. 
if __name__ == '__main__':  

    date,datadir = set_params( sys.argv[1:] ) 

    # check if archive directory is there
    if not os.path.isdir( datadir ):
        print( 'data directory ' + datadir + ' does not exists, exiting' )
        sys.exit(1)

    archive_dir = datadir + '/archive'
    if not os.path.isdir( archive_dir ):
        print( 'archive directory ' + archive_dir +' does not exists, exiting',
               file=sys.stderr, flush=True)
        sys.exit(1)
 
    # check if archive dir is writable
    if not os.access( archive_dir, os.W_OK ):
        msg = '\n ERROR: cannot write to directory:' + archive_dir
        print( msg, file=sys.stderr,flush=True )
        sys.exit(1)

    print('Data directory: ', datadir, file=sys.stderr, flush=True)
    print('Date: ', date, file=sys.stderr, flush=True)
    
    # array of sectors
    # TODO: have server report active sectors
    # TODO: address multiple years 
    
    sectors = [ 'WCONUS_B1',
                'WCONUS_B2',
                'WCONUS_B3',
                'WCONUS_B4',
                'WCONUS_C1',
                'WCONUS_C2',                  
                'WCONUS_D1',
                'WCONUS_D2',
                'WCONUS_D3',
                'WCONUS_E2' ]

    # populate sectors
    for sector in sectors:
        get_update( archive_dir, sector, date )

# end __main__

# end get_input_files.py
