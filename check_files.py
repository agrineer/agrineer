#! /usr/bin/env /usr/bin/python3

#  check_files.py
# 
#  Copyright (c) 2018 IndieCompLabs, LLC.
# 
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  (at your option) any later version.
# 
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
# 
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#  or visit https://www.gnu.org/licenses/gpl-3.0-standalone.html

check_files_copyright = 'check_files.py Copyright (c) 2018 IndieCompLabs, LLC., released under GNU GPL V3.0'

import os
import sys
import getopt
import datetime

from agrineer import wrfbase

## @file    check_files.py
## @brief   Program to check for WRF/ETo files used in GDC and SME.
## @detail  Exercises WRF/ETo data files in all sectors from 
##          archives to check that files are up to date.
##          Refer to the README in this package for instructions
##          on downloading the WRF data files.

## @author    Scott L. Williams
## @copyright Copyright (c) 2018 IndieCompLabs, LLC. All Rights Reserved.
## @license   Released under GNU General Public License V3.0.

## Usage report
def usage():
    print('usage: check_files.py', file=sys.stderr,flush=True)
    print('       -h, --help', file=sys.stderr,flush=True)
    print('       -s date, --start=date', file=sys.stderr,flush=True)
    print('       -e date, --end=date', file=sys.stderr,flush=True)
    print('       -d datadir, --dir=datadir', file=sys.stderr,flush=True)

## Parse the command line for arguments.
## @return start and end dates, if no end date is given then returns 
## day before yesterday's date.
def set_params( argv ):
    sdate = None
    edate = None
    datadir = None
    
    # TODO: add input dir as option, add begin date
    try:                                
        opts, args = getopt.getopt( argv, 'hs:e:d:', ['help', 'start=','end=','dir='] )
    except:
        usage()                          
        sys.exit(2)
                   
    for opt, arg in opts:                
        if opt in ( '-h', '--help' ):      
            usage()                     
            sys.exit(2)
            
        elif opt in ( '-d', '--dir' ):
            datadir = arg

        elif opt in ( '-s', '--start' ):
            sdate = arg

        elif opt in ( '-e', '--end' ):
            edate = arg

    if sdate == None:
        sdate = '20170101'

    if edate == None:

        # figure out day before yesterday's date
        today = datetime.datetime.now() # local time
        day_before = today - datetime.timedelta(days = 2)
        edate = day_before.strftime( "%Y%m%d" )

    if datadir == None:
        datadir = '/home/agrineer/SMVdata'

    return sdate, edate, datadir

if __name__ == '__main__':  

    sdate,edate,datadir = set_params( sys.argv[1:] )  
    print('Running from ', sdate, ' to ', edate,
          file=sys.stderr,flush=True)
    
    # array of test locations        
    locations = [ [33.25, -103.11],           # WCONUS_B1
                  [38.03, -101.28],           # WCONUS_B2
                  [44.73, -100.65],           # WCONUS_B3
                  [47.94, -101.45],           # WCONUS_B4
                  [32.25, -106.74],           # WCONUS_C1
                  [36.99, -107.19],           # WCONUS_C2                  
                  [33.92, -115.22],           # WCONUS_D1
                  [37.91, -114.97],           # WCONUS_D2
                  [43.61, -116.21],           # WCONUS_D3
                  [38.50, -121.63] ]          # WCONUS_E2

    # TODO: generate more locations as sectors increase

    w = wrfbase()             # instantiate wrfbase class
    w.load_dirs( datadir, '/tmp' )

    for coord in locations:
        
        sector = w.find_sector( coord[0], coord[1] )
        if sector == None:
            print('no sector found at:', coord[0], coord[1],
                  file=sys.stderr,flush=True)
            sys.exit(12)          # exit 12 indicates bad sector

        try:
            w.get_flist_dlist( sdate, edate, 'd03', sector )
            print('sector:', sector, 'ok.',
                  file=sys.stderr,flush=True)
        except:
            print('sector:', sector, 'has problems.',
                  file=sys.stderr,flush=True)

# end __main__

# end check_files.py
