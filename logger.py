#  logger.py
# 
#  Copyright (c) 2018 IndieCompLabs, LLC.
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  (at your option) any later version.
# 
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
# 
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#  or visit https://www.gnu.org/licenses/gpl-3.0-standalone.html

logger_copyright = 'logger.py Copyright (c) 2018 IndieCompLabs, LLC. ' + \
                   'released under GNU GPL V3.0'
import sys
import time

## @file    logger.py
## @brief   Simple message logger.
##          Writes a text message to a log file.
##
## @author    Scott L. Williams
## @copyright Copyright (c) 2018 IndieCompLabs, LLC. All Rights Reserved.
## @license   Released under GNU General Public License V3.0.

class logger(): 
    ## Constructor.
    ## @param outfile is the filepath for the log file
    def __init__( self, outfile ):

        ## logger filename
        self.filename = outfile

        # need to open and close file to append later
        # using 'w+' below in append open doesn't seem to work
        file = open( self.filename, 'w' );
        file.close()

    ## Write text to file.
    ## @param text is the string message to write
    def append( self, text ):

        # write message to log file.
        # open and close each time to preserve log
        # in case of interrupted execution.
        try:
            file = open( self.filename, 'a' );
            file.write( text )
            file.close()

        except IOError as e:
            print(e,file=sys.stderr,flush=True)

# end class logger

# end logger.py
