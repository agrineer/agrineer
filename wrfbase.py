#  wrfbase.py
# 
#  Copyright (c) 2018 IndieCompLabs, LLC.
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  (at your option) any later version.
# 
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
# 
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#  or visit https://www.gnu.org/licenses/gpl-3.0-standalone.html

wrfbase_copyright = 'wrfbase.py Copyright (c) 2018 IndieCompLabs, LLC ' + \
                    'released under GNU GPL V3.0'
import os
import sys
import datetime
from glob import glob

import numpy as np
from PIL import Image
from netCDF4 import Dataset

from agrineer import lut

## @file    wrfbase.py
## @brief   Super class for GDD snd SME projects using WRF/ETo data.
##          Reads data in NetCDF4 format from WRF 
##          (Weather, Forecast, and Research) model
##          merged with externally calculated ETo (using FAO methods).
##          See the Eto package. Also uses NetCDF4 data from static geo files. 
##
##          Depends on Python 3 versions of:\n
##          numpy - https://github.com/numpy/numpy \n
##          netCDF4 - https://github.com/Unidata/netcdf4-python \n
##          PIL - http://pythonware.com/products/pil \n
##
## @author    Scott L. Williams
## @copyright Copyright (c) 2018 IndieCompLabs, LLC. All Rights Reserved.
## @license   Released under GNU General Public License V3.0.

class wrfbase():
    ## Constructor.
    def __init__( self ):
        
        ## print verbose flag
        self.verbose = False

        # initialize variables
        self.clear_dirs()
        self.clear_geo()
        self.clear_rundates()
        
        ## logger class to write messages
        self.logger = None 

        ## soil category list.
        ## not used yet, only for report purposes. 
        ## client soil moisture models have their own designations.
        self.soilcat = [
            'None',             # our space filler so that indexes correspond
            'Sand',             # 1
            'Loamy Sand',       # 2
            'Sandy Loam',       # 3
            'Silt Loam',        # 4
            'Silt',             # 5
            'Loam',             # 6
            'Sandy Clay Loam',  # 7
            'Silty Clay Loam',  # 8
            'Clay Loam',        # 9
            'Sandy Clay',       # 10
            'Silty Clay',       # 11
            'Clay',             # 12
            'Organic Material', # 13
            'Water',            # 14
            'Bedrock',          # 15
            'Other' ]           # 16

    ## Clears input and output directory paths.
    def clear_dirs( self ):

        ## data input directory
        self.indir = None      

        ## output directory
        self.outdir = None 

    ## Checks for and sets input and output directories.
    ## @param indir - data input directory path
    ## @param outdir - result output directory path
    ## @return boolean success state (True/False)
    def load_dirs( self, indir, outdir ):

        if not os.path.isdir( indir ):
            msg = '\n ERROR: given directory: ' + indir + ' does not exist\n'
            print(msg, file=sys.stderr,flush=True)
                                
            self.clear_dirs()
            return False

        # paranoia checks follow:

        # check directory permissions
        if not os.access( indir, os.R_OK ):
            msg = '\n ERROR: cannot read directory:' + indir + '\n'
            print(msg, file=sys.stderr,flush=True)

            self.clear_dirs()
            return False

        # check if archive and static directories exist
        os.chdir( indir )

        if not os.path.isdir( 'static_3km' ):
            msg = '\n ERROR:there is no "static_3km" directory in :'+ indir
            print(msg, file=sys.stderr,flush=True)

            self.clear_dirs()
            return False
                    
        if not os.path.isdir( outdir ):
            msg = '\n ERROR: given output directory: ' + outdir + \
                  ' does not exist'
            print(msg, file=sys.stderr,flush=True)

            self.clear_dirs()
            return False

        # check write access outdir 
        if not os.access( outdir, os.W_OK ):
            msg = '\n ERROR: cannot write to directory: ' + outdir
            print(msg, file=sys.stderr,flush=True)

            self.clear_dirs()
            return False
        
        self.indir = indir
        self.outdir = outdir
        
        if self.verbose:
            print( '\n I/O directories:\n', file=sys.stderr,flush=True)
            print( '\tinput:\t\t\t\t'+ self.indir + '\n', 
                   file=sys.stderr,flush=True)
            print( '\toutput:\t\t\t\t'+ self.outdir + '\n', 
                   file=sys.stderr,flush=True)

        return True

    ## Sets and clears out geo values.
    def clear_geo( self ):

        ## site location latitude
        self.lat = None   
      
        ## site location longitude
        self.lon = None         

        ## file corresponding to given lat, lon
        self.sector_file = None 

        ## sector name
        self.sector = None      
              
        ## sector pixel x index closest to given lat, lon
        self.si = None          

        ## sector pixel y index closest to given lat, lon
        self.sj = None          

        ## prominent soil type for top layer
        self.top = None         

        ## prominent soil type for bottom layer
        self.bot = None         

        ## soil type name for top layer
        self.topcat = None      

        ## soil type name for bottom layer
        self.botcat = None

    ## Find the sector to use from given coordinates.
    ## @param lat - latitude in decimal degrees
    ## @param lon - longitude in decimal degrees
    ## @return sector and domain in filename, eg.SECTOR_geo_d0X.nc
    def find_sector( self, lat, lon ):
  
        # go to static directory (geo files) for this domain
        os.chdir( self.indir + '/static_3km' ) # FIXME: make static_3km variable

        # search active sector geo files for requested location
        for f in glob( '*.nc' ): 
            
            # open sector SMV data
            try:
                ds = Dataset( f, 'r' ) # open netcdf file

                # grab sector corner pixels' extent coordinate
                corner_lats = ds.getncattr( 'corner_lats' )
                corner_lons = ds.getncattr( 'corner_lons' )

                # pixel corner extents are given in global attributes 
                # corner_lats, corner_lons variables at array indexes (0-index):
                # 12, lower left
                # 13, upper left
                # 14, upper right
                # 15, lower right

                ll_lat = float(corner_lats[12])  # lower left corner extent
                ll_lon = float(corner_lons[12])
                
                ul_lat = float(corner_lats[13])  # upper left corner extent
                ul_lon = float(corner_lons[13])
                 
                ur_lat = float(corner_lats[14])  # upper right corner extent
                ur_lon = float(corner_lons[14]) 
                
                lr_lat = float(corner_lats[15])  # lower right corner extent
                lr_lon = float(corner_lons[15]) 

                #print f, ll_lat, ll_lon, ul_lat, ul_lon, \
                #    ur_lat, ur_lon, lr_lat, lr_lon

                # check if location is inside this sector
                # FIXME: imprecise because longitudes vary with latitude
                #        overlaps mitigate this
                if lat >= ll_lat and lat <= ur_lat:
                    if lon >= ll_lon and lon <= ur_lon:
                        return f[:-11] # keep prefix designation for sector
                                       # flexible and work backwards from end
                                       # eg.SECTOR_geo_d0X.nc
            except:
                continue        # may not be netcdf file
                
        # oh well, not in active sectors
        return None
        
    ## Locate the pixel indexed by lat,long in given sector.
    ## @param sector - sector to look in
    ## @param lat - latitude in decimal degrees
    ## @param lon - longitude in decimal degrees
    ## @return boolean success state (True/False)
    def find_pixel( self, sector, lat, lon ):

        # reconstruct data file path and open
        f = self.indir + '/static_3km/' + sector + '_geo_d03.nc'
        ds = Dataset( f, 'r' ) # open sector SMV file

        # extract lat and lon buffers from dataset
        lat_buf = ds.variables['XLAT_M'][0]
        lon_buf = ds.variables['XLONG_M'][0]

        numy,numx = lat_buf.shape # assume same lat,lon shapes

        # exhaustively search pixel centers for least distance
        # for given lat, long

        dist = float( 'inf' ) # set distance to infinity
        si = None             # sector pixel indexes
        sj = None

        # array search for pixel center least distance to given lat,long
        for j in range( 0, numy ):
            for i in range( 0, numx ):
                dely = lat - lat_buf[j,i]
                delx = lon - lon_buf[j,i]
                
                delta = dely*dely + delx*delx # no need to take square
                if delta < dist:
                    dist = delta
                    si = i
                    sj = j

        # couldn't find pixel
        if si == None or sj == None:
            return False

        # finally load location values
        self.sector_file = f
        #self.sector = os.path.basename( f[:-11] ) # FIXME: hardwired -11
        self.sector = sector
        self.si = si
        self.sj = sj
        self.lat = lat
        self.lon = lon

        # write coordinates to file for browser to use
        f = open( self.outdir + '/coords.txt', 'w' )

        f.write( '%d'%self.si + '\n' )
        f.write( "%d"%self.sj + '\n' )
        f.write( self.sector + '\n' )
        f.write( "%.8f"%self.lat + '\n' )
        f.write( "%.8f"%self.lon + '\n' )

        f.close()
        
        return True

    ## Find sector and pixel indexes.
    ## @param lat - latitude in decimal degrees
    ## @param lon - longitude in decimal degrees
    ## @return boolean success state (True/False)
    def find_location( self, lat, lon ):

        # regional sector position
        sector = self.find_sector( lat, lon )
        if sector == None:
            return False

        # identify pixel position
        return self.find_pixel( sector, lat, lon )

    ## Set location and soil values (not the same as soil input file in SME).
    ## @param lat - latitude in decimal degrees
    ## @param lon - longitude in decimal degrees
    ## @param showsoil - flag to report soil type
    ## @return boolean success state (True/False)
    def load_geo( self, lat, lon, showsoil ):

        ok = self.find_location( lat, lon )
        if not ok:            
            # report to logger
            msg = '\n ERROR: geo_panel cannot load geo values.' + \
                  '\n coordinates are not in active sectors.\n'         
            print(msg, file=sys.stderr,flush=True)
            self.logger.append( msg )
            return False

        # use previously set geo file
        ds = Dataset( self.sector_file, 'r' ) # open sector SMV file

        # use sector indexes to get dominant soil type
        self.top = int( ds.variables['SCT_DOM'][0][self.sj,self.si] )
        self.bot = int( ds.variables['SCB_DOM'][0][self.sj,self.si] )

        # express as string
        self.topcat = self.soilcat[ self.top ]
        self.botcat = self.soilcat[ self.bot ]

        # report
        self.logger.append( '\n Geo Attributes:\n' )
        self.logger.append( '  lat:\t\t' + str(self.lat) + '\n' )
        self.logger.append( '  long:\t\t' + str(self.lon) + '\n' )
        self.logger.append( '  sector:\t' + self.sector + '\n' )
        self.logger.append( '  sector x:\t' + str(self.si) + '\n' )
        self.logger.append( '  sector y:\t' + str(self.sj) + '\n' )
        if showsoil:
            self.logger.append( '\ttop soil type:\t' + self.topcat + '\n' )
            self.logger.append( '\tbot soil type:\t' + self.botcat + '\n\n' )

        return True

    ## Clear out file and date lists.
    def clear_rundates( self ):

        ## sector filename list 
        self.data_names = None
        
        ## run date list
        self.data_dates = None

    ## Make date object from date string.
    ## @param date - date object
    def makedate( self, date ):

        year = int( date[:4] )
        month = int( date[4:6] )
        day = int( date[6:8] )
        return datetime.date( year, month, day )

    ## Assemble data filenames and dates in lists.
    ## @param begin - begin date
    ## @param end - end date
    ## @param domain - WRF domain file
    ## @param sector - sector name
    ## @return a list of consecutive day data files from begin date to end date
    def get_flist_dlist( self, begin, end, domain, sector ):

        if begin > end:
            print('ERROR: begin date cannot be greater than end date\n',
                  file=sys.stderr,flush=True)
            self.logger.append( '\n ERROR: begin date cannot be greater than end date\n' )
            return None,None

        bdate = self.makedate( begin )      # get datetime begin
        edate = self.makedate( end )        # get datetime end
        ndays = (edate - bdate).days + 1    # number of days for dates,inclusive

        # create list of consecutive days between dates, inclusive
        date = bdate
        dlist = [ date.strftime('%Y%m%d') ]

        for i in range(1,ndays):
            date = date + datetime.timedelta(days=1)
            dlist.append( date.strftime('%Y%m%d') )

        # check for and load data file paths into list
        bdir = self.indir     # set base directory
        flist = []
        for date in dlist:    # using new date variable here
            
            # check for archive
            arch = self.indir + '/archive'
            if not os.path.isdir( arch ):

                self.logger.append(' archive dir: ' + arch + 
                                   ' does not exist.')
                return None,None

            # check for given sector
            sdir = arch + '/' + sector
            if not os.path.isdir( sdir ):
                self.logger.append( ' sector dir: ' +  sdir +
                                    ' does not exist.\n' )
                return None,None

            # check for given date directory
            ddir = sdir + '/' + date
            if not os.path.isdir( ddir ):
                self.logger.append( ' date dir: ' + ddir +
                                    ' does not exist.\n' )
                return None,None

            # check for file
            year = date[:4]   # get year from date
            fpath = ddir + '/' + sector +  \
                    '_SMV_' + domain + '_' +  year + '-' + date[4:6] + '-' + \
                    date[6:8] + '_06-00-00.nc'
                               
            if not os.path.isfile( fpath ):
                self.logger.append( ' file:' + fpath + ' does not exist.\n' )
                return None,None

            flist.append( fpath )

        if len(dlist) != len(flist):
            print('ERROR: number of data files does not match number of days',
                   file=sys.stderr,flush=True)
            self.logger.append(' error:number of data files does not match number of days' )
            return None,None

        return flist, dlist

    ## Checks if data are available for dates and 
    ## loads data filenames into run lists.
    ## @param begin - begin date
    ## @param end - end date
    ## @param domain - WRF domain file
    ## @param sector - sector name
    ## @return boolean success state (True/False)
    def load_filenames( self, begin, end, domain, sector ):

        # get data files starting at begin 
        flist,dlist = self.get_flist_dlist( begin, end, domain, sector )
        if flist == None:
            self.logger.append('\n cannot load data file.\n')
            return False

        self.data_names = flist
        self.data_dates = dlist

        # report files used for run
        ndays = len( self.data_names )
        if self.verbose:
            self.logger.append( '\n Datafiles used for calculations:\n' )

            for i in range( 0, ndays ):
                self.logger.append( '\t\t' + self.data_names[i] + '\n' )

        return True

    ## Convert single-banded float image to byte for display
    ## @param image - Numpy image buffer
    ## @return a Numpy byte image
    def recast_band( self, image ):
        min = np.nanmin(image)         # get values to scale by
        max = np.nanmax(image)         # ignoring nan
        
        if max == min :                # check for constant values
            scale = 0.0 	           # make image a surface plane
            c = 0.0
        else :
            # TODO: handle min=-inf,max=inf
            if np.isinf( min ):
                print("image min value=", min, file=stderr,flush=True)

            if np.isinf( max ):
                print("image max value=", max, file=stderr,flush=True)

            scale = 255.0/(max-min)    # plain linear stretch to 8-bit range
            c = -scale*min

        # supply our own resultant array of byte type
        height,width = image.shape
        b_image = np.empty( (height,width), dtype=np.uint8 )

        b_image = (image*scale).astype( np.uint8 )
        b_image = (b_image + c).astype( np.uint8 )

        return b_image

    ## Make a png image from numpy buffer.
    ## @param buf - Numpy float image buffer
    ## @param colorize - flag to use rainbow LUT
    ## @param fpath - file path of PNG image
    def save_image( self, buf, colorize, fpath ) :
        image = self.recast_band( buf ) # should be float
        image = np.flipud( image )      # wrf 0,0 lower left

        if colorize:
            l = lut.lut()
            rainbow = l.make_rainbow()
            rgb = rainbow[ image ]      # index through numpy lut
            im = Image.fromarray( rgb, mode="RGB" )
        else:
            im = Image.fromarray( image )

        im.save( fpath )

    ## Creates a flat float data file with latitude and longitude buffers.
    ## buffers are fixed size.
    ## @param data - float Numpy buffer
    ## @param geo_path - filepath to static geo files
    ## @param fpath - filepath to output file
    def save_data( self, data, geo_path, fpath ):

        ds = Dataset( geo_path, 'r' )  # open sector geo file

        # extract lat and lon buffers from dataset
        lat = ds.variables['XLAT_M'][0]
        lon = ds.variables['XLONG_M'][0]

        # construct output data file
        buf = np.empty( (171,171,3), dtype=np.float32 )
        buf[:,:,0] = data
        buf[:,:,1] = lat
        buf[:,:,2] = lon

        # wrf files use bottom left 0,0 so need to flip it
        buf = np.flipud( buf )
        buf.tofile( fpath )

# end class wrfbase

# end wrfbase.py
